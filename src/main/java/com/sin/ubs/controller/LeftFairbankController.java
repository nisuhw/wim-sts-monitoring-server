package com.sin.ubs.controller;

import javax.servlet.http.HttpServletRequest;

import org.atmosphere.cpr.AtmosphereResource.TRANSPORT;
import org.atmosphere.cpr.AtmosphereResourceEventListenerAdapter;
import org.atmosphere.cpr.Broadcaster;
import org.atmosphere.cpr.BroadcasterFactory;
import org.atmosphere.cpr.Meteor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/leftfairbank")
public class LeftFairbankController {

	private Broadcaster broadcaster = BroadcasterFactory.getDefault().get(
			getClass());

	@Scheduled(fixedDelay = 10 * 1000)
	public void startService() {
		if (!broadcaster.getAtmosphereResources().isEmpty())
			broadcaster.broadcast("{data:" + (int) (Math.random() * 1e6) + "}");
	}

	@RequestMapping("/subscribe")
	@ResponseBody
	public void subscribe(HttpServletRequest request) {
		Meteor meteor = Meteor.build(request).addListener(
				new AtmosphereResourceEventListenerAdapter());
		meteor.resumeOnBroadcast(
				meteor.transport() == TRANSPORT.LONG_POLLING ? true : false)
				.suspend(-1);
		meteor.setBroadcaster(broadcaster);
		broadcaster.addAtmosphereResource(meteor.getAtmosphereResource());
	}
}
