<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
<head>
<meta charset="utf-8">
<title>Welcome</title>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/jquery/jquery-1.9.0.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/jquery/jquery.atmosphere.js"></script>

<script type="text/javascript">
	$(function() {
		var socket = $.atmosphere
		var request = {
			url : "ws://"+document.location.host + '/leftfairbank/subscribe',
			contentType : "application/json",
			logLevel : 'TRACE',
			transport : 'websocket',
			fallbackTransport : 'long-polling',
			trackMessageLength : true,
			onOpen : function(data) {
				console.log(data);
			},
			onReconnect : function(request, response) {
				console.log(request,response);
			},
			onMessage : function (response) {
				console.log(response);
			},
			onError:function(response){
				console.log(response);
			}
		};
		var subSocket = socket.subscribe(request);
		console.log(subSocket)
	})
</script>
</head>
<body>
	<h1>see console log</h1>
</body>
</html>
